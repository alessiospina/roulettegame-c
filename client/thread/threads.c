//
//  threads.c
//  client
//
//  Created by Alessio Spina on 04/05/21.
//
#include <unistd.h>
#include <pthread/pthread.h>
#include <netinet/in.h>

#include "threads.h"
#include "../../utils/connection-setup/config.h"
#include "../utils/utils.h"



pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;


void* pollingCommunication(void *arg){
    
    pthread_mutex_lock(&mutex);
    struct sockaddr_in servaddr;
    int sockfd = connectionConfiguration(&servaddr, false);
    
    Result res = startConnection(sockfd, &servaddr);
    
    // connect the client socket to server socket
    if (connect(sockfd, (struct sockaddr_in *)&servaddr, sizeof(servaddr)) != 0) {
        printf("[pol] connection with the server failed...\n");
        pthread_exit(NULL);
    }
    else
        printf("\nconnected to the server..\n");
    pthread_mutex_unlock(&mutex);
    
    sendMessage(sockfd, "P");
    
    while (true) {
        sleep(2);
        printf("\nsend");
        sendMessage(sockfd, "1");
    }
    
    pthread_exit(NULL);
}





void* communication(void *arg){
    
    pthread_mutex_lock(&mutex);
    
    struct sockaddr_in servaddr;
    int sockfd = connectionConfiguration(&servaddr, false);
    
    if (connect(sockfd, (struct sockaddr_in *)&servaddr, sizeof(servaddr)) != 0) {
        printf("[com] connection with the server failed...\n");
        pthread_exit(NULL);
    }
    else
        printf("\nconnected to the server..\n");
    
    pthread_mutex_unlock(&mutex);
    
    
    sendMessage(sockfd, "C");
    
    pthread_exit(NULL);

}
