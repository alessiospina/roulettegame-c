//
//  threads.h
//  client
//
//  Created by Alessio Spina on 04/05/21.
//

#ifndef threads_h
#define threads_h

#include <stdio.h>

void* pollingCommunication(void *arg);
void* communication(void *arg);

#endif /* threads_h */
