//
//  utils.c
//  client
//
//  Created by Alessio Spina on 12/05/21.
//

#include <sys/socket.h>
#include <netinet/in.h>

#include "utils.h"
//#include "../../utils/models/result/result.h"


Result startConnection(int socket, struct sockaddr_in *serverAddr){
    
    if (connect(socket, (struct sockaddr *) serverAddr, sizeof(*serverAddr)) != 0) {
        return initResult(true, "[WARN] Connection Error", ERROR_CONNECT_TO_SERVER);
    }
    
    return baseResult(false, NULL);
}
