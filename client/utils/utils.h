//
//  utils.h
//  client
//
//  Created by Alessio Spina on 12/05/21.
//

#ifndef utils_h
#define utils_h

#include <stdio.h>
#include "../../utils/models/result/result.h"

Result startConnection(int socket, struct sockaddr_in *serverAddr);

#endif /* utils_h */
