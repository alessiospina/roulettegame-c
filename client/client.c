#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <pthread/pthread.h>


#include "../utils/connection-setup/config.h"
#include "./thread/threads.h"
#include "./utils/utils.h"

#define IP "127.0.0.1"
#define PORT 8080
#define SA struct sockaddr


int main(int argc, char const *argv[])
{
    /*pthread_t tid1, tid2;
    pthread_create(&tid1, NULL, pollingCommunication, NULL);
    pthread_create(&tid2, NULL, communication, NULL);
    
    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);*/
    
    Result res = NULL;
    
    struct sockaddr_in servAddr;
    int sockfd = connectionConfiguration(&servAddr, true);
    
    // ================= START CONNECTION ===================
    res = startConnection(sockfd, &servAddr);
    
    if(res->result == true){
        fprintf(stderr, "%s\n", (char *) res->payload);
        exit(-1);
    }
    
    freeResult(&res);
    // ================= END START CONNECTION ===================
    
    
    // ===================== LOGIN =======================
    // send username
    res = sendMessage(sockfd, "noname");
    
    if(res->result == true) {
        fprintf(stderr, "[ERR] Disconnect From Server: %s (Username)\n", statusString(res->status));
        exit(-1);
    }

    freeResult(&res);
    
    
    // send password
    res = sendMessage(sockfd, "alessio93");
    
    if(res->result == true) {
        fprintf(stderr, "[ERR] Disconnect From Server: %s (Password)\n", statusString(res->status));
        exit(-1);
    }
    
    freeResult(&res);
    
    
    // receive login message
    res = receiveMessage(sockfd);
    
    if(res->result == true) {
        fprintf(stderr, "[ERR] Disconnect From Server: %s\n", statusString(res->status));
        exit(-1);
    }
    
    printf("\n%s", (char *) res->payload);
    
    // ===================== END LOGIN =======================
    
    // close the socket
    close(sockfd);

    return 0;
}
