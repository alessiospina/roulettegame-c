#ifndef fileoperations_h
#define fileoperations_h

#include "../string/utilsstring.h"


extern const char* DATABASE_PATH;

bool openFile(FILE **fp, char* filename, char* mode);
bool writeOnFile(FILE *fp, char* filename, char* mode, char* textToWrite);
bool writeEntity(FILE *fp, char* filename, char** data, char** format, int n_rows);
void creationAsset(const char* file, bool isDir);
bool checkExist(const char * file);
ListString readFile(FILE *fp, char* filename);
char* readFileAndCheck(FILE *fp, char* filename, char* strToCheck, int pos);
int sizeFile(FILE *fp);

#endif