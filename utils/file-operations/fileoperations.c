#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>
#include <sys/stat.h>


#include "fileoperations.h"
#include "../string/utilsstring.h"
#include "../models/player/player.h"
#include "../models/bid/bid.h"

const char* DATABASE_PATH = "./DATABASE";


// openFile: open file and modify fp given as parameter
bool openFile(FILE **fp, char* filename, char* mode){
    FILE *p = fopen(filename, mode);
    
    if(p == NULL)
        return false;
    else {
        *fp = p;
        return true;
    }
}




// writeOnFile: write on file(opened or not) the text given as parameter
bool writeOnFile(FILE *fp, char* filename, char* mode, char* textToWrite){
    
    if (fp == NULL && (filename == NULL || mode == NULL)){
        printf("\n[WARN]\tfunction writeOnFile(FILE*, char*, char*) called improperly: (FILE* fp and char* filename cannot be both NULL.)");
        return false;
    }
        
    if(fp == NULL && !openFile(&fp, filename, mode))
        return false;

    if (fprintf(fp, "%s", textToWrite) <= 0)
        return false;

    fclose(fp);

    return true;
}



ListString readFile(FILE *fp, char* filename){
    if (fp == NULL && filename == NULL){
        printf("\n[WARN]\tfunction readFile(FILE*, char*) called improperly: (FILE* fp and char* filename cannot be both NULL.)");
        return NULL;
    }

    if(fp == NULL && !openFile(&fp, filename, "r"))
        return NULL;

    char *line = NULL;
    int index = 0, currRow = 0;
    size_t len = 0;
    ssize_t read;

    ListString list = NULL;

    while ((read = getline(&line, &len, fp)) != -1) {
        addNodeString(&list, initNodeString(line));
    }

    return list;   
}


char* readFileAndCheck(FILE *fp, char* filename, char* strToCheck, int pos){
    if (fp == NULL && filename == NULL){
        printf("\n[WARN]\tfunction readFileAndCheck(FILE*, char*) called improperly: (FILE* fp and char* filename cannot be both NULL.)");
        return NULL;
    }

    if(strToCheck == NULL){
        printf("\n[WARN]\tfunction readFileAndCheck(FILE*, char*) called improperly: (char* strToCheck cannot be NULL.)");
        return NULL;
    }

    if(fp == NULL && !openFile(&fp, filename, "r"))
        return NULL;

    char *line = NULL;
    int index = 0, currRow = 0;
    size_t len = 0;
    ssize_t read;

    while ((read = getline(&line, &len, fp)) != -1) {
         char* token = tokenize2(line, " ", 0);
         
         if (token != NULL && strcmp(token, strToCheck) == 0)
            return line;       
    }

    return NULL;   
}

// creationAsset: creates file or directory with the name give as param.
void creationAsset(const char* file, bool isDir){
    
    // SETTING TYPEFILE TO PRINT
    char *typeFile;
    
    if (isDir){
        typeFile = malloc(strlen("Directory") * sizeof(char));
        typeFile = "Directory";
    } else {
        typeFile = malloc(strlen("File") * sizeof(char));
        typeFile = "File";
    }
    
    // CHECK IF EXIST FILE/DIR
    if(!checkExist(file)){
        if(isDir)
            assert(mkdir(file, 0777) != 1);
        else { // da migliorare, poichè aprendolo con w+, apre il file e se non esite, lo crea
            FILE *p = fopen(file, "w");
            assert(p != NULL);
            fclose(p);
        }
        printf("\n[INIT DB]: %s %s created.", typeFile, file);  
    }
    else 
        printf("\n[INIT DB]: %s %s already exists.", typeFile, file);
    
}




bool checkExist(const char * file){
    
    struct stat buffer;
    int exist = stat(file, &buffer);

    if(exist == 0)
        return 1;
    else // -1
        return 0;
}





int sizeFile(FILE *fp){
    if(fp == NULL)
        return -1;
    
    fseek(fp, 0, SEEK_END); // seek to end of file
    int size = ftell(fp); // get current file pointer
    fseek(fp, 0, SEEK_SET); // seek back to beginning of file

    return size;
}



bool writeEntity(FILE *fp, char* filename, char** data, char** format, int n_rows){
    
    if(fp == NULL && filename == NULL){
        printf("\n[WARN] writeEntity invalid call because fp and filename cannot be both null");
        return false;
    }

    if(data == NULL && format == NULL){ 
        printf("\n[WARN] writeEntity failed: (data & format cannot be null)");
        return false;
    }

    
    if(fp == NULL && !openFile(&fp, filename, "a")){
        printf("\n[WARN] writeEntity failed to write the template: (Unable to open file)");
        return false;
    }

    int lenData = sizeArrayString(data, n_rows);
    int lenFormat = sizeArrayString(format, n_rows);

    int len = lenData + lenFormat + 6; // (5: 4 space + \n + \0)

    char* entityStr = calloc(len, sizeof(char));
    strcpy(entityStr, "");

    for (int i = 0; i < n_rows; i++) {
        int lenStr = strlen(data[i]) + strlen(format[i])+2;
        char *str = calloc(lenStr, sizeof(char));

        if(i == (n_rows - 1))
            snprintf(str, lenStr, "%s%s\n", format[i], data[i]);
        else
            snprintf(str, lenStr, "%s%s ", format[i], data[i]);

        entityStr = concatString(entityStr, str);
    }

    if (!writeOnFile(fp, NULL, NULL, entityStr)){
        printf("\n[WARN] writeEntity failed => writeOnFile failed");
        return false;
    }

    return true;
    
}
