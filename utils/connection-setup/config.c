#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdbool.h>
#include <arpa/inet.h>

#include "config.h"
#include "../string/utilsstring.h"
#include "../models/result/result.h"

#define PORT 8080
#define BUFFER_MAX 4
#define RESPONSE_MAX 3
#define IP "127.0.0.1"


//* Connection Setup
//* - Building struct sockadd_in parameters based on "type" variable:
//      if "type" is true => config of server sockaddr_in 
//      else => config of client sockaddr_in 
//* - Returns:
//      socketfd: id of socked created
int connectionConfiguration(struct sockaddr_in *addr, bool type){
    
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    assert(sockfd != -1);

    bzero(addr, sizeof((*addr)));

    // assign IP, PORT
    (*addr).sin_family = AF_INET;
    (*addr).sin_port = htons(PORT);

    if(type) // true => server address
        (*addr).sin_addr.s_addr = htonl(INADDR_ANY);
    else    // false => client address
        (*addr).sin_addr.s_addr = inet_addr(IP);

    return sockfd;
}


// ReceiveMessage
Result receiveMessage(int socketID){
    

    char slen [BUFFER_MAX + 1];
    memset(slen, '\0', BUFFER_MAX + 1);
    
    int byteRecv = 0;
    
    // ==> recv size of the incoming string
    if((byteRecv = (int) recv(socketID, slen, sizeof(slen), 0) <= 0)) {
        return initResult(true, NULL, ERROR_RCV_SIZE);
    }
    
    // =====> Convertion string to int of the msg size
    int len = atoi(slen);
    
    char* msg = calloc(len + 1, sizeof(char));
    
    // ==> recv the string
    if((byteRecv = (int) recv(socketID, msg, len, 0)) <= 0){
        return initResult(true, NULL, ERROR_RCV);
    }
    
    return baseResult(false, msg);
}


// Send Message
Result sendMessage(int socketID, const char* message){
    
    int len = (int) strlen(message);
    
    char slen[BUFFER_MAX + 1];
    memset(slen, '\0', BUFFER_MAX + 1);
    snprintf(slen, sizeof(slen), "%d", len);
    
    
    int byteSend = 0;
    
    if( (byteSend = (int) send(socketID, slen, sizeof(slen), 0) <= 0))
        return initResult(true, NULL, ERROR_SEND_SIZE);
    
    
    if((byteSend = (int) send(socketID, (char *) message, len, 0) <= 0 ))
        return initResult(true, NULL, ERROR_SEND);
    
    return baseResult(false, NULL);
}
