#ifndef config_h
#define config_h

#include "../models/result/result.h"

int connectionConfiguration(struct sockaddr_in *serverAddr, bool type);
Result receiveMessage(int sockedID);
Result sendMessage(int socketID, const char* message);

#endif
