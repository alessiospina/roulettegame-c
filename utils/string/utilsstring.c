#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

#include "utilsstring.h"



NodeString initNodeString(char* str){
    
    NodeString new = malloc(sizeof(struct TNodeString));
    
    if(str != NULL){
        new->data = malloc(strlen(str)* sizeof(char));
        strcpy(new->data, str);
    }

    new->next = NULL;

    return new;
}



ListString initListString(){
    ListString new = malloc(sizeof(struct TListString));
    new->top = NULL;
    return new;
}



void addNodeString(ListString *list, NodeString new){
    if(*list == NULL)
        *list = initListString();
    
    if((*list)->top == NULL) {
        (*list)->top = initNodeString(NULL);
        (*list)->top = new;
    }
    else {
        new->next = (*list)->top;
        (*list)->top = new;
    }
}



char* concatString(const char* str1, const char* str2){
    int sizeRes = strlen(str1) + strlen(str2) + 1;
    char *res = calloc(sizeRes, sizeof(char));
    strcat(res, str1);
    strcat(res, str2);
    return res;
}


char *substring(char* str, int start, int end){
    //N_ROWS = 15
    int dim = ((end - start) + 2);
    char* subbuff = malloc(dim * sizeof(char));
    memcpy(subbuff, &str[start], dim);
    subbuff[dim-1] = '\0';
    return subbuff;
}


bool containsSubString(const char* str, const char* sub){
    if (strstr(str, sub) != NULL) {
        return true;
    }
    return false;
}


char* intToString(int x) {
    int len = snprintf(NULL, 0, "%d", x);
    char* resX = malloc(len * sizeof(char));
    snprintf(resX, len+1, "%d", x);
    return resX;
}


char* floatToString(float x){
    int len = snprintf(NULL, 0, "%.2f", x);
    char* resX = malloc(len * sizeof(char));
    snprintf(resX, len+1, "%f", x);
    return resX;
}



int sizeArrayString(char **str, int n_rows){
    int res = 0;
    
    for (int i = 0; i < n_rows; i++){
        res = res + strlen(str[i]);
    }
    
    return res;
}



ListString tokenize(char* str, char delimiterR, char delimiterL){

    int currIndex = 0;
    int startIndex = 0;

    ListString list = NULL;

    for (int i = 0; i < strlen(str); i++){
        if(str[i] == delimiterR){
            startIndex = i+1;
        }
        else if (str[i] == delimiterL){
            addNodeString(&list, initNodeString(substring(str, startIndex, i-1)));
            startIndex = 0;
        }
    }

    return list;
}





char* tokenize2(char* str, char* symbol, int pos){
   
   
   char *strTemp = calloc(strlen(str), sizeof(char));
   strcpy(strTemp, str);
   

   char* token = strtok(strTemp, symbol);

   if(pos == 0)
        return token;

   int currIndex = 1;


   /* walk through other tokens */
    while( token != NULL ) {

        token = strtok(NULL, symbol);

        if(currIndex == pos)
            return token;

        currIndex++;
    }
    
    return NULL;
}

// Return curret date to string
char* dateToString(){
    char *res = calloc(80, sizeof(char));

    time_t rawtime;
    struct tm * timeinfo;

    time ( &rawtime );
    timeinfo = localtime ( &rawtime );

    strftime (res, 80, "%c", timeinfo);

    return res;
}
