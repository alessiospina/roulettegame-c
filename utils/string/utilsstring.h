#ifndef utilsstring_h
#define utilsstring_h

struct TNodeString {
    char* data;
    struct TNodeString *next;
} TNodeString;

typedef struct TNodeString* NodeString;

struct TListString {
    NodeString top;
} TListString;

typedef struct TListString* ListString;


NodeString initNodeString(char* str);
ListString initListString();
void addNodeString(ListString *list, NodeString new);


char* concatString(const char* str1, const char* str2);
char* substring(char* str, int start, int end);
bool containsSubString(const char* str, const char* sub);
char* intToString(int x);
char* floatToString(float x);
int sizeArrayString(char **str, int n_rows);
ListString tokenize(char* str, char delimiterR, char delimiterL);
char* tokenize2(char* str, char* symbol, int pos);
char* dateToString();

#endif