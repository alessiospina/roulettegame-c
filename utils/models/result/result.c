#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "result.h"



Result initResult(bool result, void* payload, StatusResult status){
    
    Result e = malloc(sizeof(struct TResult));

    e->result = result;
    e->payload = payload;
    e->status = status;

    return e;       
}


Result baseResult(bool result, void* payload){
    Result e = malloc(sizeof(struct TResult));
    e->result = result;
    e->payload = payload;
    e->status = SUCCESS;

    return e;
}


void freeResult(Result *r, bool freePayload){
    if(*r == NULL)
        return;
    
    if(freePayload == true && (*r)->payload != NULL)
        free((*r)->payload);
    
    free(*r);
    *r = NULL;
}



char* statusString(StatusResult s){
    
    char *res = NULL;
    
    switch (s) {
        case NAN:
            res = calloc(strlen("NAN") + 1, sizeof(char));
            strcpy(res, "NAN");
            break;
            
        case SUCCESS:
            res = calloc(strlen("SUCCESS") + 1, sizeof(char));
            strcpy(res, "SUCCESS");
            break;
        
        case ERROR_SEND_SIZE:
            res = calloc(strlen("ERROR_SEND_SIZE") + 1, sizeof(char));
            strcpy(res, "ERROR_SEND_SIZE");
            break;
            
        case ERROR_RCV_SIZE:
            res = calloc(strlen("ERROR_RCV_SIZE") + 1, sizeof(char));
            strcpy(res, "ERROR_RCV_SIZE");
            break;
            
        case ERROR_SEND:
            res = calloc(strlen("ERROR_SEND") + 1, sizeof(char));
            strcpy(res, "ERROR_SEND");
            break;
            
        case ERROR_RCV:
            res = calloc(strlen("ERROR_RCV") + 1, sizeof(char));
            strcpy(res, "ERROR_RCV");
            break;
            
        case ERROR_PASSW:
            res = calloc(strlen("ERROR_PASSW") + 1, sizeof(char));
            strcpy(res, "ERROR_PASSW");
            break;
            
        case ERROR_USER:
            res = calloc(strlen("ERROR_USER") + 1, sizeof(char));
            strcpy(res, "ERROR_USER");
            break;
            
        case ERROR_CONNECT_TO_SERVER:
            res = calloc(strlen("ERROR_CONNECT_TO_SERVER") + 1, sizeof(char));
            strcpy(res, "ERROR_CONNECT_TO_SERVER");
            break;
    }
    
    return res;
}
