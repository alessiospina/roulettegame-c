#ifndef error_h
#define error_h

#include <stdbool.h>



enum EStatusResult {
    NAN = 1,
    SUCCESS = 0,
    ERROR_SEND_SIZE = -1,
    ERROR_RCV_SIZE = -2,
    ERROR_SEND = -3,
    ERROR_RCV = -4,
    ERROR_PASSW = -5,
    ERROR_USER = -6,
    ERROR_CONNECT_TO_SERVER = -7,
};

typedef enum EStatusResult StatusResult;

struct TResult {
    bool result;
    void* payload;
    StatusResult status;
} TResult;

typedef struct TResult* Result;


Result initResult(bool result, void* payload, StatusResult status);
Result baseResult(bool result, void* payload);
void freeResult(Result *r, bool freePayload);
char* statusString(StatusResult s);

#endif
