#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<stdbool.h>

#include "bid.h"
#include "../../file-operations/fileoperations.h"
#include "../../string/utilsstring.h"

#define N_PARAMETER_BID 3

const char* BID_DB_PATH = "/BID";
const char* FORMAT_FILE_BID[] = {"G:", "D:", "A:"};



Bid initBid(float amount, char* id_game){
    Bid newB = malloc(sizeof(struct TBid));

    newB->amount = amount;

    newB->id_game = id_game;
    
    char* date = dateToString();
    int len = (int) strlen(date) + 1;
    newB->date = calloc(len, sizeof(char));
    strcpy(newB->date, date);    
    
    newB->next = NULL;

    return newB;
}


// AddBidList: Add bid to a list
void addBidList(Bid *list, Bid newBid) {
    newBid->next = *list;
    (*list) = newBid;
}



Bid deleteBidListHelper(Bid list, Bid b){
    if(list != NULL){
        if(isEqual(list, b)){
            Bid tmp = list->next;
            free(list);
            return tmp;
        }
        list->next = deleteBidListHelper(list, b);
    }
    return list;
}




// DeleteBidList: Delete b from list of bids
void deleteBidList(Bid *list, Bid b){
    *list = deleteBidListHelper(*list, b);  
}




// PopBidList remove and returns last bid added (head of list)
Bid popBidList(Bid *list){
    Bid ret = *list;
    Bid tmp = (*list)->next;
    free(*list);
    (*list) = tmp;
    return ret;
}


bool isEqual(Bid a, Bid b){
    if(a->amount == b->amount && (difftime(mktime(&(a->date)), mktime(&(b->date))) == 0) && strcmp(a->id_game, b->id_game) == 0)
        return true;
    else
        return false;
}


char** bidToArrString(Bid b){
    char **res = malloc(N_PARAMETER_BID * sizeof(char *));
    int len = 0;

    for (int i = 0; i < N_PARAMETER_BID; i++){
        switch(i){
            case 0:
                len = (int) strlen(b->id_game) + 3;
                res[i] = calloc(len, sizeof(char));
                snprintf(res[i], len, "[%s]", b->id_game);
                break;
            
            case 1:
                len = (int) strlen(b->date) + 3;
                res[i] = calloc(len, sizeof(char));
                snprintf(res[i], len, "[%s]", b->date);
                break;
            
            case 2:
                len = (int) strlen(floatToString(b->amount)) + 3;
                res[i] = calloc(len, sizeof(char));
                snprintf(res[i], len, "[%s]", floatToString(b->amount));
                break;
        }
    }

    return res;  
}




bool writeBidOnFile(Bid b, char* username, char* path){
    if (b == NULL){
        printf("\n[WARN] writeBidOnFile failed: Player p cannot be null");
        return false;
    }

    char** bidArrStr = bidToArrString(b);
    int len = snprintf(NULL,0, "%s%s/%s.txt", path, BID_DB_PATH, username);
    char* pathBidFile = calloc(len, sizeof(char));
    snprintf(pathBidFile,len+1, "%s%s/%s.txt", path, BID_DB_PATH, username);
    

    if(!writeEntity(NULL, pathBidFile, bidArrStr, FORMAT_FILE_BID, N_PARAMETER_BID)){
        printf("\n[WARN] writePlayerOnFile => writeEntity failed");
        return false;
    }

    return true;
}





Bid parseBidFromString(char *bidString){

    ListString list = tokenize(bidString, '[', ']');
    Bid newB = initBid(0.0, NULL);


    int index = 0;

    while(list->top != NULL){
        switch(index){
            case 0:
                newB->amount = atof(list->top->data);
                break;
            case 1: 
                newB->date = list->top->data;
                break;
            case 2:
                newB->id_game = list->top->data;
                break;
        }
        list->top = list->top->next;
        index++;
    }

    return newB;   
}


ListString readBidsFromFile(char* database_path, char* username){
    int len = snprintf(NULL,0, "%s%s/%s.txt", database_path, BID_DB_PATH, username);
    char* pathBidFile = calloc(len, sizeof(char));
    snprintf(pathBidFile,len+1, "%s%s/%s.txt", database_path, BID_DB_PATH, username);

    ListString list = readFile(NULL, pathBidFile);   

    return list;
}

