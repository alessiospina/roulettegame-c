#ifndef bid_h
#define bid_h

#include<time.h>
#include<stdbool.h>

#include "../../string/utilsstring.h"

extern const char* BID_DB_PATH;
extern const char* FORMAT_FILE_BID[];


struct TBid {
    float amount;
    char *id_game;
    char *date;
    struct TBid* next;
};

typedef struct TBid* Bid;

Bid initBid(float amount, char* id_game);
void addBidList(Bid *list, Bid b);
bool isEqual(Bid a, Bid b);
void deleteBidList(Bid *list, Bid b);
Bid deleteBidListHelper(Bid list, Bid b);
Bid popBidList(Bid *list);
char** bidToArrString(Bid b);
bool writeBidOnFile(Bid b, char* username, char* path);
Bid parseBidFromString(char *bidString);
ListString readBidsFromFile(char* database_path, char* username);

#endif
