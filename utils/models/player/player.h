
#ifndef player_h
#define player_h

#include "../bid/bid.h"
#include "../../string/utilsstring.h"

struct TPlayer {
    char *name;
    char *surname;
    char *username;
    char *email;
    char *password;
    int socketID_C;
    int socketID_P;
    Bid listBids;
};

typedef struct TPlayer *Player;

struct TPlayerList {
    Player *listPlayers;
    int indexSize;
    int maxSize;
};

typedef struct TPlayerList *PlayerList;

extern const char* PLAYER_DB_PATH;
extern const char *FORMAT_FILE_PLAYER[];

//Single Entity Management
Player initPlayer(char* name, char* surname, char* username, char* email, int socketID, char* password);
void addBidToPlayer(Player p, Bid b);
Bid popBidFromPlayer(Player p);
void removeBidFromPlayer(Player p, Bid b);
void freePlayer(Player *p);
Player findPlayerByUsername(PlayerList list, const char* username);


//Parsing & Utils
Player parsePlayerFromString(char *player);
char* playerToString(Player p);
int getLenStringPlayer(Player p);
char** playerToArrString(Player p);
bool writePlayerOnFile(Player p, char* path);
bool loadPlayerList(PlayerList list, ListString users, int* n_players);
bool registerPlayer(Player p, char* database_path);

//List Players Management
PlayerList initPlayerList(int num_max);
void addPlayerToList(PlayerList list, Player newP);
void printPlayerList(PlayerList list);
#endif
