#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<assert.h>

#include "player.h"
#include "../bid/bid.h"
#include "../../file-operations/fileoperations.h"
#include "../../string/utilsstring.h"

#define N_PARAMETER_PLAYER 5

const char* PLAYER_DB_PATH = "/players.txt";
const char* FORMAT_FILE_PLAYER[] = {"U:", "N:", "C:", "E:", "P:"};

// Player creations
Player initPlayer(char* name, char* surname, char* username, char* email, int socketID, char* password) {
    
    Player newP = malloc(sizeof(struct TPlayer));
    
    newP->name = calloc(strlen(name) + 1, sizeof(char));
    strcpy(newP->name, name);
    
    newP->surname = calloc(strlen(surname) + 1, sizeof(char));
    strcpy(newP->surname, surname);
    
    newP->username = calloc(strlen(username) + 1, sizeof(char));
    strcpy(newP->username, username);
    
    newP->password = calloc(strlen(password) + 1, sizeof(char));
    strcpy(newP->password, password);
    
    newP->email = calloc(strlen(email) + 1, sizeof(char));
    strcpy(newP->email, email);
    
    newP->socketID_C = socketID;
    
    newP->socketID_P = -1;
    
    newP->listBids = NULL;

    return newP;
}


// AddBidToPlayer
void addBidToPlayer(Player p, Bid b){
    addBidList(&(p->listBids), b);
}


//Remove and return last done Bid 
Bid popBidFromPlayer(Player p){
    return popBidList(&(p->listBids));
}

// Remove specific bid from list
void removeBidFromPlayer(Player p, Bid b){
    return deleteBidList(&(p->listBids), b);
}


Player parsePlayerFromString(char *player){
    
    char *name = NULL;
    char *surname = NULL;
    char *email = NULL;
    char *username = NULL;
    char *password = NULL;
    
    ListString list = tokenize(player, '[', ']');
    Player newP;

    int index = 0;

    while(list->top != NULL){
        switch(index){
            case 0:
                password = calloc(strlen(list->top->data) + 1, sizeof(char));
                strcpy(password, list->top->data);
                break;
            case 1:
                email = calloc(strlen(list->top->data) + 1, sizeof(char));
                strcpy(email, list->top->data);
                break;
            case 2:
                surname = calloc(strlen(list->top->data) + 1, sizeof(char));
                strcpy(surname, list->top->data);
                break;
            case 3:
                name = calloc(strlen(list->top->data) + 1, sizeof(char));
                strcpy(name, list->top->data);
                break;
            case 4:
                username = calloc(strlen(list->top->data) + 1, sizeof(char));
                strcpy(username, list->top->data);
                break;
        }
        list->top = list->top->next;
        index++;
    }
    
    newP = initPlayer(name, surname, username, email, 0, password);
    
    free(name);
    free(surname);
    free(username);
    free(email);
    free(password);
    
    return newP;
    
}



char** playerToArrString(Player p){
    char** res = malloc(N_PARAMETER_PLAYER * sizeof(char*));
    int len = 0;

    for(int i = 0; i < N_PARAMETER_PLAYER; i++){
        switch (i) {
        
        case 0: 
            len = (int) strlen(p->username) + 3;
            res[i] = calloc(len, sizeof(char));
            snprintf(res[i], len, "[%s]", p->username);
            break;
        case 1:
            len = (int) strlen(p->name) + 3;
            res[i] = calloc(len, sizeof(char));
            snprintf(res[i], len, "[%s]", p->name);
            break;
        case 2:
            len = (int) strlen(p->surname) + 3;
            res[i] = calloc(len, sizeof(char));
            snprintf(res[i], len, "[%s]", p->surname);
            break;
        case 3:
            len = (int) strlen(p->email) + 3;
            res[i] = calloc(len, sizeof(char));
            snprintf(res[i], len, "[%s]", p->email);
            break;
        case 4:
            len = (int) strlen(p->password) + 3;
            res[i] = calloc(len, sizeof(char));
            snprintf(res[i], len, "[%s]", p->password);
            break;

        default:
            break;
        }
    }

    return res;
}



// playerToString() convert Player p to a string with FORMAT
char* playerToString(Player p){
    if (p == NULL) 
        return NULL;
    
    // =========== Length Calculation =============
    int lenPlayerStr = getLenStringPlayer(p); // get len calculated by the sum of elements of Player parameters

    if(lenPlayerStr < 0)
        return NULL;
    
    int n_rows = sizeof(FORMAT_FILE_PLAYER)/sizeof(*FORMAT_FILE_PLAYER); // n_rows of format
    int lenFormat = sizeArrayString((char **) FORMAT_FILE_PLAYER, n_rows); // len of format

    int len = lenPlayerStr + lenFormat + 6; // 1 \n , 4 space and final char
    // ========================================

    char* res = calloc(len, sizeof(char)); 

    //can be generalized
    snprintf(res, len, "%s%s %s%s %s%s %s%s %s%s\n", FORMAT_FILE_PLAYER[0], p->username, 
                                                     FORMAT_FILE_PLAYER[1], p->name, 
                                                     FORMAT_FILE_PLAYER[2], p->surname,
                                                     FORMAT_FILE_PLAYER[3], p->email,
                                                     FORMAT_FILE_PLAYER[4], p->password);

    return res;
}






// getLenStringPlayer: returns the sum of strlen of each element of Player struct
int getLenStringPlayer(Player p){
    if (p == NULL) 
        return -1;

    return (int) strlen(p->name) + (int) strlen(p->surname) + (int) strlen(p->email) + (int) strlen(p->username) + (int) strlen(p->password);
}





bool writePlayerOnFile(Player p, char* path){
    if (p == NULL){
        printf("\n[WARN] writePlayerOnFile failed: Player p cannot be null");
        return false;
    }

    char** playerArrStr = playerToArrString(p);

    if(!writeEntity(NULL, concatString(path, PLAYER_DB_PATH), playerArrStr, FORMAT_FILE_PLAYER, N_PARAMETER_PLAYER)){
        printf("\n[WARN] writePlayerOnFile => writeEntity failed");
        return false;
    }

    return true;
}


// RegisterPlayer: creates file username.txt in BID
bool registerPlayer(Player p, char* database_path){
    if(p == NULL){
        printf("\n[WARN] registerPlayer => p cannot be null");
        return false;
    }
    
    if (!writePlayerOnFile(p, database_path)){
        return false;
    }
    
    int len = (int)(strlen(database_path) + strlen(BID_DB_PATH) + strlen(p->username) + strlen("/") + strlen(".txt") + 1);
    char* path = calloc(len, sizeof(char));
    snprintf(path, len, "%s%s/%s.txt", database_path, BID_DB_PATH, p->username);
    creationAsset(path, false);

    return true;
}


void freePlayer(Player *p){
    free((*p)->name);
    free((*p)->surname);
    free((*p)->email);
    free((*p)->username);
    free((*p)->password);
    free(*p);
    *p = NULL;
}



PlayerList initPlayerList(int num_max){
    PlayerList list = malloc(sizeof(PlayerList));
    list->indexSize = -1;
    list->maxSize = num_max;
    list->listPlayers = calloc(num_max, sizeof(struct TPlayer));

    return list;
}





void addPlayerToList(PlayerList list, Player newP){
    assert(list != NULL);    
    assert((list->indexSize + 1) <= list->maxSize);

    list->listPlayers[list->indexSize + 1] = newP;
    list->indexSize++;
}



bool loadPlayerList(PlayerList list, ListString users, int* n_players){
    
    if(list == NULL && n_players == NULL){
        printf("[WARN] loadPlayerList failed: list & n_players cannot be both null");
        return false;
    }
    
    if (users == NULL){
        printf("[WARN] loadPlayerList failed: users cannot be null");
        return false;
    }

    if(list == NULL)
        list = initPlayerList(*n_players);
    
    
     while (users->top != NULL){
        Player player = parsePlayerFromString(users->top->data);
        addPlayerToList(list, player);
        users->top = users->top->next;
    }

    return true;
}


void printPlayerList(PlayerList list){
    assert(list != NULL); 

    printf("\nPLAYER LIST:");

    for (int i = 0; i <= list->indexSize; i++) {
       printf("\n[%d] (Player - NAME: %s, SURNAME: %s, USERNAME: %s, EMAIL: %s, PASSWORD: %s)", i+1, list->listPlayers[i]->name, list->listPlayers[i]->surname, list->listPlayers[i]->username, list->listPlayers[i]->email, list->listPlayers[i]->password);
    }
    
    printf("\n");
}


Player findPlayerByUsername(PlayerList list, const char* username){
    if(username == NULL || list->indexSize <= 0)
        return NULL;
    
    for (int i = 0; i <= list->maxSize; i++) {
        if(strcmp(list->listPlayers[i]->username, username) == 0){
            return list->listPlayers[i];
        }
    }
    
    return NULL;
}
