//
//  global.h
//  server
//
//  Created by Alessio Spina on 01/05/21.
//

#ifndef global_h
#define global_h

#include "../../../utils/models/player/player.h"

#define MAX 80
#define PORT 8080
#define NMAX_PLAYER 126
#define SA struct sockaddr

extern PlayerList playerList;

extern pthread_mutex_t mutex;

extern pthread_cond_t condition;

#endif /* global_h */
