//
//  global.c
//  server
//
//  Created by Alessio Spina on 01/05/21.
//

#include <stdio.h>
#include <pthread/pthread.h>
#include "global.h"
#include "../../../utils/models/player/player.h"


// Player List contains all player connected to the server
PlayerList playerList = NULL;

// mutex for the race condition
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

// mutex condition for the race condition
pthread_cond_t condition = PTHREAD_COND_INITIALIZER;

