#ifndef threads_h
#define threads_h

void* connection_client_handler(void *args);
void* game(void* arg);
void* betManager(void *arg);
void* polling(void *arg);
#endif
