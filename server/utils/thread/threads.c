
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread/pthread.h>
#include <sys/socket.h>
#include <unistd.h>

#include "threads.h"
#include "../../../utils/connection-setup/config.h"
#include "../../../utils/string/utilsstring.h"
#include "../utils.h"
#include "../global/global.h"

void* connection_client_handler(void *args){
    
    int clientID = *(int *) args;
    
    Result operationRes = receiveMessage(clientID);
    
    if (operationRes->result == true) {
        fprintf(stderr, "\n[WARN] Client (%d) disconnect with error: %s", clientID, statusString(operationRes->status));
        freeResult(&operationRes, true);
        
        pthread_exit(NULL);
    }
    
    
    char *msg = calloc(strlen((char *)operationRes->payload) + 1, sizeof(char));
    strcpy(msg, (char *) operationRes->payload);
    
    freeResult(&operationRes, true);
    
    if(strcmp(msg, "C") == 0){ // communication client socket
        free(msg);
        
        Result res = loginOperation(clientID);
        
        if(res->result == true){
            
            fprintf(stderr, "\n[LOGIN-WARN] Client (%d) failed to login with error: %s", clientID, statusString(res->status));
            
            pthread_exit(NULL);
        }
        else {
            Player p = (Player) res->payload;
            fprintf(stderr, "\n[LOGIN] Client (%s) is logged. ", p->username);
        }
        
        freeResult(&res, false);

        printPlayerList(playerList);
        
    }
    else if (strcmp(msg, "P")){ // polling client socket
        free(msg);
        
        Result res = receiveMessage(clientID);
        
        if(res->result == true){
            fprintf(stderr, "\n[POLLING-WARN] Client (%d) failed to connect with error: %s", clientID, statusString(res->status));
            
            freeResult(&res, true);
            
            pthread_exit(NULL);
        }
        
        
        Player p = findPlayerByUsername(playerList, (char *) res->payload);
        p->socketID_P = clientID;
        
        pthread_t tidPolling;
        pthread_create(&tidPolling, NULL, polling, &p->socketID_P);
        
        freeResult(&res, true);
        
    }
    else { // error message
        fprintf(stderr, "\n[CONNECTION HANDLER WARN] Client (%d) unknown message: %s", clientID, msg);
        free(msg);
    }
    
    
    pthread_exit(NULL);
}








void* game(void* arg){
    
    // da migliorare => potrebbe essere di grandezza playerlist.size e poi gestire la grandezza con la realloc
    pthread_t id_bets_comunication[NMAX_PLAYER];
    
    while (playerList->indexSize != 0) {
        printf("\n[GAME THREAD]: waiting player...");
        pthread_cond_wait(&condition, &mutex);
    }
    
    pthread_mutex_unlock(&mutex);
    
    printf("\n[GAME THREAD]: Started...");
    
    while (playerList->indexSize > 0) {
        printf("\n[GAME THREAD] BETS OPENED");
        for (int i = 0; i < playerList->indexSize; i++) {
            if(playerList->listPlayers[i]->socketID_P > 0)
                pthread_create(&id_bets_comunication[i], NULL, betManager, playerList->listPlayers[i]);
        }
    }
    
    
    pthread_exit(NULL);
}



void* betManager(void *arg){
    
    Player p = (Player)arg;
    Result res = NULL;
    
    
    res = sendMessage(p->socketID_C, "SEND BETS");
    
    if (res->result == false){
        freeResult(&res, true);
        pthread_exit(NULL);
    }
    
    freeResult(&res, true);
    
    res = receiveMessage(p->socketID_C);
    
    if(res->result == false){
        freeResult(&res, true);
        pthread_exit(NULL);
    }
    
    float amount = atof((char *)res->payload);
    freeResult(&res, true);
    
    Bid b = initBid(amount, "id");
    
    pthread_mutex_lock(&mutex);
    addBidList(&p->listBids, b);
    pthread_mutex_unlock(&mutex);
    
    
    
    pthread_exit(NULL);
}




void* polling(void *arg){
    
    Player p = (Player) arg;
    int clientSocket = p->socketID_C;
    
    Result res = receiveMessage(clientSocket);
    
    char* resMsg = (char *) res->payload;
    
    printf("\n%d %s", clientSocket, (char *) res->payload);
    
    if(strcmp(resMsg, "P") == 0){
        while (true) {
            res = receiveMessage(clientSocket);
            printf("\n%d %s", clientSocket, (char *) res->payload);
            
            if(res->result == true)
                break;
        }
        
    }
    
    
    pthread_exit(NULL);
}
