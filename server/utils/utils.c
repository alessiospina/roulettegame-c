#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include <time.h>
#include <pthread/pthread.h>

#include "utils.h"
#include "./global/global.h"
#include "./../../utils/string/utilsstring.h"
#include "./../../utils/models/result/result.h"
#include "./../../utils/models/player/player.h"
#include "./../../utils/file-operations/fileoperations.h"
#include "./../../utils/connection-setup/config.h"



#define SA struct sockaddr


// startConnection: setup bind(), listen and servAddr
void startConnection(int sockfd,  struct sockaddr_in servAddr){
    // Binding newly created socket to given IP and verification
    if ((bind(sockfd, (SA*)&servAddr, sizeof(servAddr))) != 0) {
        printf("\nsocket bind failed...\n");
        exit(0);
    }
    else
        printf("\nSocket successfully binded..\n");
  
    // Now server is ready to listen and verification
    if ((listen(sockfd, 5)) != 0) {
        printf("\nListen failed...\n");
        exit(0);
    }
    else
        printf("\nServer listening..\n");

}


// acceptConnection: allows client to connect and return the socket client
int acceptConnection(int sockfd, struct sockaddr_in **clientAddr){
    
    socklen_t len = sizeof(*clientAddr);

    int connfd = accept(sockfd,(SA *)(*clientAddr), &len);
    assert(connfd >= 0);
    printf("\nserver acccept the client...");

    return connfd;
}


// creationDatabaseFiles: Creates directory and files to store persistenty data.
void creationDatabaseFiles(const char* DATABASE_PATH, const char* PLAYER_DB_PATH, const char*  BID_DB_PATH, const char* LOGGER_PATH){

    //dir creation (if not exist)
    creationAsset(DATABASE_PATH, true);

    //player txt file creation (if not exist)
    char* playerFilePath = concatString(DATABASE_PATH, PLAYER_DB_PATH);
    creationAsset(playerFilePath, false);

    //bid dir creation (if not exist)
    char* bidFilePath = concatString(DATABASE_PATH, BID_DB_PATH);
    creationAsset(bidFilePath, true);

    //log txt file creation (if not exist)
    char* logFilePath = concatString(DATABASE_PATH, LOGGER_PATH);
    creationAsset(logFilePath, false);

}




Result loginServer(char *username, char* password){

    if (username == NULL || password == NULL)
        return initResult(true, NULL, NAN);
    
    int len = snprintf(NULL, 0, "%s", username) + 5;
    char* str = calloc(len,  sizeof(char));
    snprintf(str, len, "%s[%s]", FORMAT_FILE_PLAYER[0], username);

    char* player = readFileAndCheck(NULL, concatString(DATABASE_PATH, PLAYER_DB_PATH), str, 0); 
    
    if(player == NULL)
        return initResult(true, NULL, ERROR_USER);

    ListString pass = tokenize(player, '[', ']');

    if(strcmp(pass->top->data, password) != 0)
        return initResult(true, NULL, ERROR_PASSW);;

    return baseResult(false, player);
}




Result loginOperation(int clientID){
    
    char* user = NULL;
    char* passw = NULL;
    
    Result res = NULL;
    
    // ==== RECEIVE USERNAME ====
    res = receiveMessage(clientID);
    
    if(res->result == true)
        return res;
    
    user = calloc(strlen((char *) res->payload) + 1, sizeof(char));
    strcpy(user, (char *) res->payload);
    freeResult(&res, true);
    
    // ==== RECEIVE PASSWORD ====
    res = receiveMessage(clientID);
    
    if(res->result == true){
        free(user);
        return res;
    }
    
    passw = calloc(strlen((char *) res->payload) + 1, sizeof(char));
    strcpy(passw, (char *) res->payload);
    freeResult(&res, true);
    
    
    // ==== LOGIN PHASE ====
    res = loginServer(user, passw);
    
    // it's ok, user found
    if (res->result == false) {
        
        // string of date to send to the user
        char* userDate = calloc(strlen((char *) res->payload) + 1, sizeof(char));
        strcpy(userDate, (char *) res->payload);
        
        freeResult(&res, true);
        
        // send the date to the user
        res = sendMessage(clientID, userDate);
        
        if(res->result == true){
            //fprintf(stderr, "\n[WARN] Client %d disconnect from Server: (%s)", clientID, statusString(res->status));
            //freeResult(&res);
            free(userDate);
            free(user);
            free(passw);
            
            return res;
        }
        
        Player p = parsePlayerFromString(userDate);
        p->socketID_C = clientID;
        res->payload = p;
        
        // LOCK & ADD PLAYER
        pthread_mutex_lock(&mutex);
        addPlayerToList(playerList, p);
        pthread_cond_signal(&condition);
        pthread_mutex_unlock(&mutex);
        
        //freeResult(&res);
        free(userDate);
        free(user);
        free(passw);
        return res;
    }
    else { // user not found
        StatusResult s = res->status;
        freeResult(&res, true);
        res = sendMessage(clientID, intToString(s));
        free(user);
        free(passw);
        return res;
    }
}



bool registration(char* username, char* email, char* name, char* surname, char* password){
    return false;
}





