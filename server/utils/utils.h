#ifndef utils_h
#define utils_h

#include "./../../utils/models/result/result.h"

void startConnection(int sockfd,  struct sockaddr_in servAddr);
int acceptConnection(int sockfd, struct sockaddr_in **clientAddr);

void creationDatabaseFiles(const char*, const char*, const char*, const char*);
bool checkExist(const char * file);

char* concatString(const char* str1, const char* str2);
void creationAsset(const char* file, bool isDir);


Result loginServer(char *username, char* password);
Result loginOperation(int clientID);


#endif
