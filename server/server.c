#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include <time.h>
#include <pthread/pthread.h>

#include "./utils/utils.h"
#include "../utils/file-operations/fileoperations.h"
#include "../utils/connection-setup/config.h"
#include "../utils/models/bid/bid.h"
#include "../utils/models/result/result.h"
#include "../utils/models/player/player.h"
#include "../utils/models/logger/logger.h"
#include "../utils/string/utilsstring.h"
#include "./utils/thread/threads.h"
#include "./utils/global/global.h"



int main(int argc, char const *argv[])
{
    int sockfd;
    struct sockaddr_in servAddr;
    struct sockaddr_in *clientAddr;
    
    pthread_t tid;
    pthread_t tGame;

    creationDatabaseFiles(DATABASE_PATH, PLAYER_DB_PATH, BID_DB_PATH, LOGGER_DB_PATH);
    
    sockfd = connectionConfiguration(&servAddr, true);
    startConnection(sockfd, servAddr);
    
    playerList = initPlayerList(NMAX_PLAYER);
    
    
    pthread_create(&tGame, NULL, game, NULL);

    while(true){
        int clientfd=acceptConnection(sockfd, &clientAddr);
        //pthread_create(&tid, NULL, connectionAccepted, &clientfd);
        pthread_create(&tid, NULL, connection_client_handler, &clientfd);
        // => genererò un thread per ogni connessione ricevuta
    }

    // After chatting close the socket
    close(sockfd);

    return 0;
}


//======================= EXAMPLE FUNCTION CALL ==============================

//  openFile(&p, concatString(DATABASE_PATH, PLAYER_DB_PATH), "a");

//  writeOnFile(NULL, concatString(DATABASE_PATH, PLAYER_DB_PATH), "ciao mamma1");
//  writeOnFile(p, NULL, "ciao mamma2");
//  writeOnFile(NULL, NULL, "ciao mamma3");

//  addPlayerToList(list, initPlayer("Alessio", "Spina", "we", "we", "we"));

//char** resultFile = readFile(NULL, concatString(DATABASE_PATH, PLAYER_DB_PATH));

//writeTemplateFile(concatString(DATABASE_PATH, PLAYER_DB_PATH), "N_ROWS = ", 5);

//p = fopen(concatString(DATABASE_PATH, PLAYER_DB_PATH), "r+");
//updateTemplate(p);


//ListString listString = NULL;
//addNodeString(&listString, initNodeString("Ciao0"));

//parsePlayerFromString("U:[noname] N:[Alessio] C:[Spina] E:[alessiospina93@hotmail] P:[alalalalab]");

// Player pl = initPlayer("Alessio", "Spina", "noname", "alessiospina93@hotmail.it", "alalalalal");
// writePlayerOnFile(pl, DATABASE_PATH);


//Player pl = initPlayer("Alessio", "Spina", "fabrifibra", "alessiospina93@hotmail.it", "alalalalal");
//Bid b = initBid(250.0, "X123");
//registerPlayer(pl, DATABASE_PATH);
//writeBidOnFile(b, pl->username, DATABASE_PATH);
    

//Player pl = initPlayer("Alessio", "Spina", "noname", "alessiospina93@hotmail.it", "alalalalal");
//registerPlayer(pl, DATABASE_PATH);
//writePlayerOnFile(pl, DATABASE_PATH);

//ListString users = readFile(NULL, concatString(DATABASE_PATH, PLAYER_DB_PATH));
//ListString bidsString = readBidsFromFile(DATABASE_PATH, pl->username);

//while (bidsString->top != NULL){
    //Bid bi = parseBidFromString(bidsString->top->data);
    //bidsString->top = bidsString->top->next;
//}
    
//printPlayerList(list);
//parsePlayerFromString("U:[noname] N:[Alessio] C:[Spina] E:[alessiospina93@hotmail] P:[alalalalab]");
//char** res = playerToArrString(pl);
//p = fopen(concatString(DATABASE_PATH, PLAYER_DB_PATH), "a");
//writePlayerOnFile(p, pl);
